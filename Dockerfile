# tag::production[]
FROM scottw/alpine-perl:5.32.0
RUN apk add fortune
RUN curl -L https://cpanmin.us | perl - -M https://cpan.metacpan.org -n Mojolicious
WORKDIR /app
COPY main.pl /app/main.pl
COPY templates /app/templates

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/bin/sh", "-c", "morbo /app/main.pl --listen http://0.0.0.0:8080"]
# end::production[]
