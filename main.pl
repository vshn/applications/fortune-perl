#!/usr/bin/env perl
use Mojolicious::Lite -signatures;

my $port = 8080;
my $version = '1.2-perl';
my $hostname = `hostname`;

# tag::router[]
get '/' => sub ($c) {
    my $fortune = `fortune`;
    my $number = int(rand(1000));
    $c->respond_to(
        json => { json => {
            'version' => $version,
            'number' => $number,
            'message' => $fortune,
            'hostname' => $hostname}
        },
        html => sub {
            $c->stash(version => $version);
            $c->stash(hostname => $hostname);
            $c->stash(message => $fortune);
            $c->stash(number => $number);
            $c->render(template => 'fortune');
        },
        any => {
            text => sprintf("Fortune %s cookie of the day #%d:\n\n%s", $version, $number, $fortune)
        }
    );
};
# end::router[]

app->start;
